﻿bm_2000_test = {
	start_date = 1022.1.1
	is_playable = yes

	character = {
		name = "bookmark_test_marion_silmuna"
		dynasty = dynasty_silmuna
		dynasty_splendor_level = 1
		type = male
		birth = 1001.2.19
		title = k_dameria
		government = feudal_government
		culture = damerian
		religion = cult_of_the_dame
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_EASY"
		history_id = 1
		position = { 765 590 }

		animation = disapproval
	}
}
